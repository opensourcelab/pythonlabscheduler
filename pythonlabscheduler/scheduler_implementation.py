import importlib
import pkgutil
import traceback
from typing import List, Type, Dict, Optional, Iterable
import os
import time
from threading import Lock
from pythonlabscheduler.solver_interface import JSSPSolver, AlgorithmInfo
from pythonlabscheduler.structures import Machine, Operation, JSSP, Schedule, MoveOperation
import pythonlabscheduler.solvers
import logging
from pythonlabscheduler.scheduler_interface import SchedulerInterface

from pythonlabscheduler.utility import parse_jobshop_from_yaml_file

class Scheduler(SchedulerInterface):
    jssp_solver: JSSPSolver
    available_solvers_by_name: Dict[str, Type[JSSPSolver]]

    def __init__(self):
        self.inject_solvers()
        self.computation_lock = Lock()
        # change the following lines to pick other default algorithm
        self.select_algorithm('BottleneckPD')
        #self.select_algorithm('CP-Solver')
        # load a default job-shop configuration
        yaml_file_path = os.path.join(os.path.dirname(__file__), '..', 'tests', 'test_data',
                                        'lab_config_example.yaml')
        # the file lara_platform_config is not part of the repository
        if os.path.isfile(yaml_file_path):
            with open(yaml_file_path, 'r') as reader:
                content = reader.read()
                job_shop = parse_jobshop_from_yaml_file(content)
                self.configure_job_shop(machine_list=job_shop)

    def inject_solvers(self):
        """
        Searches for classes implementing the JSSPSolver interface in all modules in the solvers/ directory.
        Any matching Class is added as a solver and made available.
        :return:
        """
        pck = pythonlabscheduler.solvers
        self.available_solvers_by_name = dict()
        for finder, name, ispkg in pkgutil.iter_modules(pck.__path__):
            mod_name = pck.__name__ + "." + name
            # some solvers might not be loadable because of missing requirements
            try:
                submodule = importlib.import_module(mod_name)
                if ispkg:
                    pass
                else:
                    for attr in submodule.__dir__():
                        try:
                            buff = getattr(submodule, attr)
                            if issubclass(buff, JSSPSolver):
                                solver_name = buff.get_algorithm_info().name
                                self.available_solvers_by_name[solver_name] = buff
                                print(f"found solver {buff}")
                        except:
                            # print(traceback.print_exc())
                            # there will be lots of errors to ignore because not all attributes are callable
                            pass
            except Exception as ex:
                logging.warning(f"module {mod_name} could not be loaded: {ex}")
                pass

    def configure_job_shop(self, machine_list: List[Machine]):
        self.job_shop = machine_list

    def select_algorithm(self, algorithm_name: str) -> bool:
        """
        Changes the current algorithm of the solver. The names of all available algorithms can be requested
        via the available_algorithms attribute.
        :param algorithm_name: Name of the chosen algorithm.
        :return: Returns whether there is an algorithm with the given name
        """
        if algorithm_name not in self.available_solvers_by_name:
            logging.error(f"Solver named {algorithm_name} not found")
            return False
        try:
            # create an instance of the selected solver type
            self.jssp_solver = self.available_solvers_by_name[algorithm_name]()
        except Exception as ex:
            print(ex, traceback.print_exc())
            return False
        return True

    @property
    def available_algorithms(self) -> List[AlgorithmInfo]:
        algo_metadata_list = [solver.get_algorithm_info() for solver in self.available_solvers_by_name.values()]
        return algo_metadata_list

    @property
    def current_algorithm_info(self) -> AlgorithmInfo:
        return self.jssp_solver.get_algorithm_info()

    def compute_schedule(self, operations: Iterable[Operation], computation_time: float) -> Optional[Schedule]:
        try:
            start = time.time()
            self.computation_lock.acquire()
            jssp = JSSP(operations=operations, machines=self.job_shop)
            # compute the schedule
            jssp.add_dummys()
            logging.info(f"Computing schedule for {len(list(operations))} operations")
            print(f"Computing schedule for {len(list(operations))} operations")
            schedule = self.jssp_solver.compute_schedule(jssp, computation_time, computation_time)
            if schedule:
                jssp.remove_dummys(schedule)
                self._enforce_precedences(schedule)
                self._enforce_min_capacities(list(operations), schedule)
            logging.info(f"Computation took {time.time() - start} seconds.")
            print(f"Computation took {time.time() - start} seconds.")
            #for k, a in schedule.items():
            #    print(k, a)
        finally:
            self.computation_lock.release()
        return schedule

    def _enforce_precedences(self, schedule: Schedule):
        """
        Adds machine precedences between steps that definitively need to be executed without overlapping.
        This is already implicitly given by the schedule, but adding it explicitly might help executing the schedule.
        :param schedule:
        :return:
        """
        # get the names of all devices with a capacity of one
        machine_names = [machine.name for machine in self.job_shop if machine.max_capacity == 1]
        for name in machine_names:
            # get all the steps on this device
            steps = [idx for idx, assign in schedule.items() if assign.machines_to_use['main'] == name]
            sorted_steps = sorted(steps, key=lambda idx: schedule[idx].start)
            for step1, step2 in zip(sorted_steps[:-1], sorted_steps[1:]):
                schedule[step2].machine_precedences.append(step1)

    def _enforce_min_capacities(self, operations: List[Operation], schedule: Schedule):
        """
        Searches for movements, that are necessary for certain operations due to minimum capacities.
        Adds machine precedence constraints between those
        :param schedule:
        :return:
        """
        try:
            # sort all movements by start time
            movements = [op for op in operations if isinstance(op, MoveOperation)]
            sorted_moves = sorted(movements, key=lambda m: schedule[m.name].start)
            machine_by_name: Dict[str, Machine] = {m.name: m for m in self.job_shop}
            for idx, assignment in schedule.items():
                main_name = assignment.machines_to_use['main']
                main = machine_by_name[main_name]
                if main.min_capacity > 1:
                    # filter all movements, that involve the main device and start before this operation
                    relevant_moves = [m for m in sorted_moves
                                      if main_name in schedule[m.name].machines_to_use.values()
                                      and schedule[m.name].start < schedule[idx].start]
                    # find the last movement, that gets the min capacity fulfilled and enforce precedence
                    curr_load = 0
                    deciding_filler = None
                    # will be all loadings since the last unloading
                    load_moves = []
                    for move in relevant_moves:
                        # elegant way to cover the edge case when both origin and target are main_name
                        change = int(schedule[move.name].machines_to_use['target'] == main_name) \
                                 - int(schedule[move.name].machines_to_use['origin'] == main_name)
                        if curr_load < main.min_capacity <= curr_load + change:
                            deciding_filler = move
                        if change >= 0:
                            load_moves.append(move)
                        else:
                            load_moves = []
                        curr_load += change
                    if deciding_filler:
                        logging.debug(f"for operation {idx}, we have deciding filler {deciding_filler.name}"
                                      f" and loading operations {[m.name for m in load_moves]}")
                        if main.allows_overlap:
                            # enforce precedence of the deciding filler
                            schedule[idx].machine_precedences.append(deciding_filler.name)
                        else:
                            # if interrupts are not allowed also enforce precedence to all loadings since last unloading
                            for move in load_moves:
                                schedule[idx].machine_precedences.append(move.name)
                    else:
                        logging.warning(f"The device executing {idx} seems to be not sufficiently filled")
        except Exception as ex:
            logging.error(f"In _enforce_min_capacities{ex}\n{traceback.print_exc()}")
