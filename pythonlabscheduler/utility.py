"""
A collection of useful functions to use the pythonlabscheduler.
"""

from typing import List

from pythonlabscheduler.structures import Machine, MoveOperation, Operation, RequiredMachine
import yaml
import logging
import traceback
import datetime


def parse_jobshop_from_yaml_file(yaml_file: str) -> List[Machine]:
    try:
        config_dict = yaml.safe_load(yaml_file)
        pythonLab_translation = {}
        for name, pl_name in config_dict['pythonlab_translation'].items():
            pythonLab_translation[name] = pl_name
        job_shop = []
        for device_type, device_list in config_dict['sila_servers'].items():
            device_class = pythonLab_translation[device_type]
            for device_name, param_dict in device_list.items():
                max_capacity = param_dict['capacity']
                if 'min_capacity' in param_dict:
                    min_capacity = param_dict['min_capacity']
                else:
                    min_capacity = 1
                process_capacity = param_dict['process_capacity'] if "process_capacity" in param_dict\
                    else max_capacity
                allows_overlap = bool(param_dict['allows_overlap']) if 'allows_overlap' in param_dict else True
                job_shop.append(Machine(name=device_name,
                                        max_capacity=max_capacity,
                                        type=device_class,
                                        min_capacity=min_capacity,
                                        process_capacity=process_capacity,
                                        allows_overlap=allows_overlap))
        logging.info("Available instruments:")
        for m in job_shop:
            logging.info(m)
        return job_shop
    except Exception as ex:
        print(ex, traceback.print_exc())
        raise ex


def create_operations_from_json(workflow_graph):
    operation_by_id = dict()
    for node in workflow_graph[0]:
        idx = node[0]
        duration = node[1]
        start = None if node[3] == "None" else datetime.fromisoformat(node[3])
        finish = None if node[4] == "None" else datetime.fromisoformat(node[4])
        requirements = [RequiredMachine(type=rm[0], tag=rm[1],
                                        preferred=None if rm[2] == 'None' else rm[2])
                        for rm in node[2]]
        is_movement = 'target' in [rm.tag for rm in requirements]
        if is_movement:
            operation = MoveOperation(name=idx, duration=duration, required_machines=requirements,
                                      start=start, finish=finish)
        else:
            operation = Operation(name=idx, duration=duration, required_machines=requirements,
                                  start=start, finish=finish)
        operation_by_id[idx] = operation
    for edge in workflow_graph[1]:
        u = edge[1]
        v = edge[0]
        operation_by_id[v].preceding_operations.append(u)
        # for stability reasons we want wait_costs
        operation_by_id[v].wait_cost[u] = edge[2]
        operation_by_id[v].max_wait[u] = edge[3]
        min_wait = 0 if not edge[4] else edge[4]
        operation_by_id[v].min_wait[u] = min_wait
    return operation_by_id
