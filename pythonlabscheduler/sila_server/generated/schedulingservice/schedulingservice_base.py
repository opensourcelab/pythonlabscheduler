# Generated by sila2.code_generator; sila2.__version__: 0.10.4
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, List

from sila2.server import FeatureImplementationBase, MetadataDict, ObservableCommandInstance

from .schedulingservice_types import (
    AlgorithmMetaData,
    ComputeSchedule_Responses,
    SelectAlgorithm_Responses,
    WorkflowGraph,
)

if TYPE_CHECKING:
    from ...server import Server


class SchedulingServiceBase(FeatureImplementationBase, ABC):
    parent_server: Server

    def __init__(self, parent_server: Server):
        """
            Provides an interface for the PythonLabOrchestrator or any other lab environment to interact with the
        scheduling module. It provides an observable command to schedule a workflow(exact syntax explained in command).
        You can choose between different algorithms as get their metadata.
        """
        super().__init__(parent_server=parent_server)

    @abstractmethod
    def get_AvailableAlgorithms(self, *, metadata: MetadataDict) -> List[AlgorithmMetaData]:
        """
        List of algorithm info for all available algorithms

        :param metadata: The SiLA Client Metadata attached to the call
        :return: List of algorithm info for all available algorithms
        """
        pass

    @abstractmethod
    def get_CurrentAlgorithm(self, *, metadata: MetadataDict) -> AlgorithmMetaData:
        """
        The currently selected algorithm

        :param metadata: The SiLA Client Metadata attached to the call
        :return: The currently selected algorithm
        """
        pass

    @abstractmethod
    def SelectAlgorithm(self, AlgorithmName: str, *, metadata: MetadataDict) -> SelectAlgorithm_Responses:
        """
        Selects the algorithm to be used in the ComputeSchedule command


        :param AlgorithmName: Name of the Algorithm

        :param metadata: The SiLA Client Metadata attached to the call

        """
        pass

    @abstractmethod
    def ComputeSchedule(
        self,
        WorkflowGraph: WorkflowGraph,
        MaxComputationTime: float,
        *,
        metadata: MetadataDict,
        instance: ObservableCommandInstance,
    ) -> ComputeSchedule_Responses:
        """
          Takes a workflow graph (see parameter description) and computes a schedule for the currently configured lab
        environment(LabConfigurationController) with the currently selected algorithm.
        You have to give the maximum computation time.


          :param WorkflowGraph: The workflow graph of an experiment in the required SiLA structure.

          :param MaxComputationTime: Maximum computation time in seconds

          :param metadata: The SiLA Client Metadata attached to the call
          :param instance: The command instance, enabling sending status updates to subscribed clients

          :return:

              - Schedule: The computed schedule in the defined SiLA structure. The schedule is empty if no valid schedule was found.


        """
        pass
