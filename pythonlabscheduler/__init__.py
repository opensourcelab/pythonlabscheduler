"""Top-level package for pythonLab Scheduler."""

__author__ = """mark doerr"""
__email__ = "mark.doerr@uni-greifswald.de"
__version__ = "0.1.0"
