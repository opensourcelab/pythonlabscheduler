"""
Collection of utility structures used by the CP solver
"""

import logging
from ortools.sat.python.cp_model import CpModel, IntervalVar, CpSolver, IntVar
from typing import Union, Dict, List, Tuple
from datetime import datetime
from dataclasses import dataclass, field
from pythonlabscheduler.structures import JSSP, Operation


@dataclass
class IntervalBundle:
    """
    Contains all information about the interval variables corresponding to an operation.
    """
    op_idx: str  # name of the operation
    # list of interval vars together with a dictionary {tag: machine_name} to specify the executors and a is_present
    # variable(which might be a fix integer or an actual variable)
    interval_vars: List[Tuple[IntervalVar, Dict[str, str], Union[bool, IntVar]]] = field(default_factory=list)

    def __str__(self):
        return f"{self.op_idx}:\n" + "\n".join(str(tupl[1]) for tupl in self.interval_vars)


class CPVariables:
    operation_by_machine: Dict[str, List[IntervalVar]]
    reference_time: datetime
    offset: int
    # some generous upper bound for the makespan
    horizon: int
    intervals: Dict[str, IntervalBundle]
    # provides the is_present variable to interval variable_name
    presence = Dict[str, Union[int, IntVar]]

    def __init__(self, inst: JSSP, offset: int):
        self.operation_by_machine = {name: list() for name in inst.machine_collection.machine_by_id.keys()}
        self.reference_time = datetime.now()
        self.horizon = 2**31-1
        self.offset = offset
        self.intervals = dict()
        self.presence = dict()

    def intervals_by_id(self, idx: str) -> List[IntervalVar]:
        intervals = []
        for interval, roles, is_present in self.intervals[idx].interval_vars:
            intervals.append(interval)
        return intervals

    @property
    def all_intervals(self) -> List[IntervalVar]:
        intervals = []
        for bundle in self.intervals.values():
            for interval, roles, is_present in bundle.interval_vars:
                intervals.append(interval)
        return intervals

    def add_interval(self, o: Operation, interval: IntervalVar, is_present: Union[int, IntVar] = 1, **kwargs):
        """
        Adds the interval var into the correct bundle.
        In the kwargs must be specified which machine shall execute which required role
        if that is not already specified in the operation (i.e. for pooling).
        """
        # create an entry if necessary
        if o.name not in self.intervals:
            self.intervals[o.name] = IntervalBundle(o.name)
        roles = dict()
        for required in o.required_machines:
            # the executing machine is either fixed by the operation....
            if required.preferred:
                roles[required.tag] = required.preferred
            # ... or specified in the kwargs
            else:
                if required.tag not in kwargs:
                    logging.error(f"The executor for {required} in operation {o.name} is unclear")
                else:
                    roles[required.tag] = kwargs[required.tag]
        # add the tuple of interval-var and role assignments
        self.intervals[o.name].interval_vars.append((interval, roles, is_present))
        self.operation_by_machine[roles["main"]].append(interval)
        # links variable name and is_present variable
        self.presence[interval.name] = is_present


def print_stats(solver: CpSolver, cp: CpModel):
    print("\nStatistics")
    print(f"  - conflicts: {solver.num_conflicts}")
    print(f"  - branches : {solver.num_branches}")
    print(f"  - wall time: {solver.wall_time}s")
    print(cp.model_stats())
