"""
A solver implementation using google OR-tools to model and solve the JSSP as a constraint program(CP)
"""
import logging
import traceback
import time
from ortools.sat.cp_model_pb2 import CpSolverStatus
from ortools.sat.python.cp_model import CpModel, OPTIMAL, FEASIBLE, IntervalVar, CpSolver, IntVar
from typing import Optional, List, Tuple
from datetime import datetime, timedelta
from threading import Timer
from math import ceil, floor
from pythonlabscheduler.structures import JSSP, Schedule, ScheduledAssignment, MoveOperation, Operation, RequiredMachine
from pythonlabscheduler.solver_interface import JSSPSolver, AlgorithmInfo
from pythonlabscheduler.solvers.cp_solver_structures import CPVariables

# the balance between makespan and waiting costs
ALPHA = 10


class CPSolver(JSSPSolver):
    def compute_schedule(self, inst: JSSP, time_limit: float, offset: float, **kwargs) -> Optional[Schedule]:
        try:
            model, vars = self.create_model(inst, offset)
            solver, state = self.solve_cp(model, time_limit=time_limit)
            if state in [OPTIMAL, FEASIBLE]:
                schedule = self.extract_schedule(solver, vars, inst)
            else:
                schedule = None
            return schedule
        except:
            logging.error(traceback.print_exc())

    def is_solvable(self, inst: JSSP) -> bool:
        model, vars = self.create_model(inst, 0)
        # TODO: there is a method in OR-tools to check solvability
        state = self.solve_cp(model, 5)
        return state in [OPTIMAL, FEASIBLE]

    @staticmethod
    def get_algorithm_info() -> AlgorithmInfo:
        return AlgorithmInfo(
            name="CP-Solver",
            is_optimal=True,
            success_guaranty=True,
            max_problem_size=60,
        )

    @staticmethod
    def create_model(inst: JSSP, offset: float) -> Tuple[CpModel, CPVariables]:
        model = CpModel()
        vars = CPSolver.create_variables(model, inst, ceil(offset))
        CPSolver.set_objective(model, vars, inst)
        CPSolver.add_precedence_constraints(model, vars, inst)
        CPSolver.add_processing_capacity_constraints(model, vars, inst)
        CPSolver.add_spacial_capacity_constraints(model, vars, inst)
        CPSolver.add_load_while_processing_constraints(model, vars, inst)
        return model, vars

    @staticmethod
    def solve_cp(cp: CpModel, time_limit: float) -> Tuple[CpSolver, CpSolverStatus]:
        print("start solving....", end='', flush=True)
        start = time.time()
        solver = CpSolver()
        # enforce the time limit
        Timer(interval=time_limit, function=solver.stop_search).start()
        status = solver.Solve(cp)
        print(f"... done. result: {CpSolverStatus.Name(status)} (obj:{solver.objective_value})"
              f" after {round(time.time() - start, 2)}seconds")
        return solver, status

    @staticmethod
    def extract_schedule(solver: CpSolver, vars: CPVariables, inst: JSSP) -> Schedule:
        schedule = dict()
        for idx, bundle in vars.intervals.items():
            for var, roles, is_present in bundle.interval_vars:
                used = solver.value(is_present)
                # get the found start time
                start_value = solver.value(var.start_expr())
                logging.debug(f"{var.name} is used: {bool(used)}, start: {start_value}, end: {solver.value(var.end_expr())}")
                if used:
                    # convert it to datetime using the reference time
                    start = vars.reference_time + timedelta(seconds=start_value)
                    schedule[idx] = ScheduledAssignment(start=start, machines_to_use=roles)
        return schedule

    @staticmethod
    def create_interval_var(lb: int, ub: int, o: Operation, cp: CpModel, ref_time: datetime,
                            optionality: Optional[IntVar] = None) -> IntervalVar:
        """
        Utility method to create and add a new interval variable to the model. Returns the created variable.
        """
        if optionality is not None:
            name = optionality.name
        else:
            name = f"I_{o.name}"
        if o.start:
            start = round((o.start-ref_time).total_seconds())
        else:
            start = cp.new_int_var(lb, ub, name=f"start_{o.name}")
        if o.finish:
            end = round((o.finish-ref_time).total_seconds())
            size = end - start
        else:
            size = ceil(o.duration)
            end = cp.new_int_var(lb, ub, name=f"end_{o.name}")

        if optionality is not None:
            interval_var = cp.new_optional_interval_var(start=start, size=size, end=end, name=name,
                                                        is_present=optionality)
        else:
            interval_var = cp.new_interval_var(start=start, size=size, end=end, name=name)
        return interval_var

    @staticmethod
    def create_variables(cp: CpModel, inst: JSSP, offset: int) -> CPVariables:
        vars = CPVariables(inst, offset)
        pooling_operations = []
        for id, o in inst.operations_by_id.items():
            ambiguous_executors = [r for r in o.required_machines if not r.preferred]
            if ambiguous_executors:
                pooling_operations.append(o)
                if len(ambiguous_executors) > 1:
                    logging.warning(f"We can not yet handle operations with multiple ambiguous executors:"
                                    f" {o.required_machines}")
            else:
                var = CPSolver.create_interval_var(offset, vars.horizon, o, cp, vars.reference_time)
                vars.add_interval(o, var)
        # handle all the operations which have multiple machines to possibly be executed by (flexible JSSP)
        CPSolver.handle_pooling(cp, vars, pooling_operations, inst)
        return vars

    @staticmethod
    def handle_pooling(cp: CpModel, vars: CPVariables, operations: List[Operation], inst: JSSP):
        """
        Creates the part of the model which assigns executing machines to operations which can be executed by different
        machines.
        """
        # Add optional intervals for every possible executor
        for op in operations:
            # find the ambiguous executor(s)...
            # ... we currently assume, there is only one such required machine
            for ambiguous in [rm for rm in op.required_machines if not rm.preferred]:
                # iterate over all possible executors
                for machine in inst.machine_collection.machines_by_class[ambiguous.type]:
                    # create and add a matching optional interval
                    is_present = cp.new_int_var(0, 1, name=f"{op.name}_{ambiguous.tag}->{machine.name}")
                    interval = CPSolver.create_interval_var(vars.offset, vars.horizon, op, cp, vars.reference_time,
                                                            optionality=is_present)
                    vars.add_interval(op, interval, **{ambiguous.tag: machine.name}, is_present=is_present)
            # enforce exactly one of those intervals to be chosen
            is_present_vars = [t[2] for t in vars.intervals[op.name].interval_vars]
            cp.add_exactly_one(is_present_vars)

        # for subsequent operations, the main/target of the first must match the source/main of the second
        def relevant_machine(op: Operation, is_first: bool) -> RequiredMachine:
            if isinstance(op, MoveOperation):
                if is_first:
                    return op.target_machine
                else:
                    return op.origin_machine
            else:
                return op.main_machine

        # find all pairs of subsequent operations where matching executors must be enforced
        for idx, op in inst.operations_by_id.items():
            for idx_prev in op.preceding_operations:
                op_prev = inst.operations_by_id[idx_prev]
                machine_post = relevant_machine(op, is_first=False)
                machine_prev = relevant_machine(op_prev, is_first=True)
                if not (machine_prev.preferred and machine_post.preferred):
                    for i1, roles_prev, is_present_prev in vars.intervals[idx_prev].interval_vars:
                        for i2, roles_post, is_present_post in vars.intervals[idx].interval_vars:
                            # check whether the relevant executing machines match
                            if roles_prev[machine_prev.tag] == roles_post[machine_post.tag]:
                                # either both intervals must be chosen or none
                                cp.add(is_present_prev == is_present_post)

    @staticmethod
    def set_objective(cp: CpModel, vars: CPVariables, inst: JSSP):
        makespan = cp.new_int_var(lb=0, ub=vars.horizon, name="makespan")
        cp.add_max_equality(
            makespan,
            [interval.end_expr() for interval in vars.all_intervals],
        )
        # create the variable for total waiting costs
        max_wait_cost = ceil(max(max(op.wait_cost.values()) for op in inst.operations_by_id.values() if op.wait_cost))
        # we need some upper bound
        wc_ub = vars.horizon * max_wait_cost
        wait_cost = cp.new_int_var(lb=0, ub=wc_ub, name="makespan")

        # create waiting cost variables for every precedence constraint
        wait_costs_per_op = []
        for idx, op in inst.operations_by_id.items():
            if op.wait_cost:
                for second in vars.intervals_by_id(idx):
                    for idx_o, cost_per_second in op.wait_cost.items():
                        wc = cp.new_int_var(lb=0, ub=wc_ub, name=f"wc_{idx}->{idx_o}")
                        #TODO: this gives wrong waiting costs for pooling operations
                        for first in vars.intervals_by_id(idx_o):
                            cp.add(wc >= (second.start_expr() - first.end_expr()) * round(cost_per_second))
                        wait_costs_per_op.append(wc)
        # add costs for waiting until start
        for idx in inst.start_operation_ids():
            # when it has already started this is redundant
            if not inst.operations_by_id[idx].start:
                wc = cp.new_int_var(lb=0, ub=wc_ub, name=f"wc_{idx}-start")
                for interval in vars.intervals_by_id(idx):
                    wait_to_start_costs = round(inst.operations_by_id[idx].wait_to_start_costs)
                    cp.add(wc >= interval.start_expr() * wait_to_start_costs)
                wait_costs_per_op.append(wc)
        # sum up all waiting costs
        cp.add_abs_equality(wait_cost, sum(wait_costs_per_op))
        # set the objective
        cp.minimize(ALPHA * makespan + wait_cost)

    @staticmethod
    def add_precedence_constraints(cp: CpModel, vars: CPVariables, inst: JSSP):
        for idx, o in inst.operations_by_id.items():
            for idx_prev in o.preceding_operations:
                for prev_interval in vars.intervals_by_id(idx_prev):
                    for post_interval in vars.intervals_by_id(idx):
                        min_wait = ceil(o.min_wait[idx_prev])
                        cp.add(min_wait <= post_interval.start_expr() - prev_interval.end_expr())
                        max_wait = floor(min(o.max_wait[idx_prev], vars.horizon))
                        # only add a constraint when relevant
                        if max_wait < vars.horizon:
                            cp.add(post_interval.start_expr() - prev_interval.end_expr() <= max_wait)

    @staticmethod
    def add_processing_capacity_constraints(cp: CpModel, vars: CPVariables, inst: JSSP):
        for name, machine in inst.machine_collection.machine_by_id.items():
            operations_on_machine = vars.operation_by_machine[name]
            if operations_on_machine:
                # handle maximum capacity constraints
                if machine.process_capacity == 1:
                    cp.add_no_overlap(operations_on_machine)
                else:
                    cp.add_cumulative(operations_on_machine, capacity=machine.process_capacity,
                                      demands=[1 for op in operations_on_machine])
                # handle minimum capacity constraints
                if machine.min_capacity > 1:
                    num_inactive_intervale = ceil(len(operations_on_machine) / machine.min_capacity) + 1
                    actives = [1]
                    level_changes = [- machine.min_capacity]
                    times = [0]
                    # model the changes to/from inactivity as optional level changes
                    for i in range(num_inactive_intervale):
                        actives.append(cp.new_int_var(0, 1, name=f"start_inactive_{machine.name}_{i}_p"))
                        actives.append(cp.new_int_var(0, 1, name=f"end_inactive_{machine.name}_{i}_p"))
                        level_changes += [machine.max_capacity, - machine.max_capacity]
                        times.append(cp.new_int_var(0, vars.horizon, name=f"start_inactive_{machine.name}_{i}"))
                        times.append(cp.new_int_var(0, vars.horizon, name=f"end_inactive_{machine.name}_{i}"))
                    for interval in operations_on_machine:
                        is_present = vars.presence[interval.name]
                        actives += [is_present, is_present]
                        level_changes += [1, -1]
                        times += [interval.start_expr(), interval.end_expr()]
                    cp.add_reservoir_constraint_with_active(
                        times=times, level_changes=level_changes, actives=actives,
                        min_level=0, max_level=machine.max_capacity - machine.min_capacity
                    )

    @staticmethod
    def add_spacial_capacity_constraints(cp: CpModel, vars: CPVariables, inst: JSSP):
        machine_names = inst.machine_collection.machine_by_id.keys()
        start_occupation = inst.start_occupations()
        changes = {name: [] for name in machine_names}
        times = {name: [] for name in machine_names}
        actives = {name: [] for name in machine_names}
        for idx, bundle in vars.intervals.items():
            if isinstance(inst.operations_by_id[idx], MoveOperation):
                for interval, roles, is_present in bundle.interval_vars:
                    origin = roles["origin"]
                    target = roles["target"]
                    # during the move both locations are considered occupied
                    times[origin].append(interval.end_expr())
                    times[target].append(interval.start_expr())
                    changes[target].append(1)
                    changes[origin].append(-1)
                    # by adding is_present to the actives, the solver ignores non-present intervals
                    actives[target].append(is_present)
                    actives[origin].append(is_present)
        for name in machine_names:
            capacity = inst.machine_collection.machine_by_id[name].max_capacity
            cp.add_reservoir_constraint_with_active(times=[0] + times[name],
                                                    level_changes=[start_occupation[name]] + changes[name],
                                                    actives=[1] + actives[name],
                                                    min_level=-1000, max_level=capacity)

    @staticmethod
    def add_load_while_processing_constraints(cp: CpModel, vars: CPVariables, inst: JSSP):
        for name, machine in inst.machine_collection.machine_by_id.items():
            if not machine.allows_overlap:
                for idx, op in inst.operations_by_id.items():
                    # iterate over all move operations
                     if isinstance(op, MoveOperation):
                         for move_interval, roles, is_present in vars.intervals[idx].interval_vars:
                             # check whether the move involves this machine
                             if name in [roles['origin'], roles['target']]:
                                 # iterate over all operations on this machine
                                 for op_interval in vars.operation_by_machine[name]:
                                    # add pair wise no-overlap constraints
                                    cp.add_no_overlap([op_interval, move_interval])

    @staticmethod
    def add_group_constraints(cp: CpModel, vars: CPVariables, inst: JSSP):
        """
        Not strictly necessary in the sense of the JSSP definition. We define a group of operations as an operation
        together with all its directly preceding options which must be more than one. The constraints added in this
        function enforce all operations of a group to be started before another one can be started.

        """
        pass
