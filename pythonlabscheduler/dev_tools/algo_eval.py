import json
import argparse
from typing import List, Tuple


from pythonlabscheduler.structures import Schedule, Machine, JSSP
from pythonlabscheduler.scheduler_implementation import Scheduler
from os import path
from pythonlabscheduler.utility import parse_jobshop_from_yaml_file, create_operations_from_json
import pandas as pd
import time


def get_lab() -> List[Machine]:
    lab_config_file = path.join(path.abspath(path.dirname(__file__)), "..", "..",
                                "tests", "test_data", "sila_server_config_changed.yml")
    with open(lab_config_file, 'r') as reader:
        return parse_jobshop_from_yaml_file(reader.read())


def run_test_series(algorithm_name: str, test_instances: List[str], time_limit: int)\
    -> Tuple[List[Schedule], List[float]]:
    scheduler = Scheduler()
    scheduler.select_algorithm(algorithm_name)
    scheduler.configure_job_shop(get_lab())
    results = []
    times = []
    for filename in test_instances:
        if filename.endswith('json'):
            with open(filename, 'r') as reader:
                sila_wfg = json.load(reader)
            #if len(sila_wfg[0]) > algo_info.max_problem_size:
            #    print(f"skipping {filename} because it is too big")
            #    continue
            print(f"testing {filename.replace('.json', '')}")
            op_by_id = create_operations_from_json(sila_wfg)
            start_computation = time.time()
            schedule = scheduler.compute_schedule(op_by_id.values(), time_limit)
            times.append(time.time() - start_computation)
            results.append(schedule)
    return results, times


def createJSSPs(files: List[str]):
    instances = []
    for filename in files:
        with open(filename, 'r') as reader:
            sila_wfg = json.load(reader)
        op_by_id = create_operations_from_json(sila_wfg)
        jssp = JSSP(op_by_id.values(), get_lab())
        instances.append(jssp)
    return instances


def parse_command_line():
    """ Looking for command line arguments"""
    parser = argparse.ArgumentParser(description="Algorithm evaluation")
    test_data_dir = path.join(path.abspath(path.dirname(__file__)), "..", "..", "tests", "test_data")
    parser.add_argument(
        "-d", "--test_data", action="store", default=test_data_dir)
    return parser.parse_args()

def make_latex_table(df: pd.DataFrame):
    # round
    # add artificial column
    # change to "time"
    # add midrules
    # scale large numbers
    # remove inst_name

    df.to_latex("algo_eval_table.tex")

if __name__ == "__main__":
    args = parse_command_line()
    algos = [
             "NaivMIP",
             "BottleneckPD",
             "CP-Solver",
             "RandomPD",
             "LPTFPD",
             ]
    real = ["SolvabilityAssay", "Repetitive4PlateProcess", "KWETransaminaseAssay",
            "CultivationHarvest", "Repetitive12Plates", "Kinetic2Plates"]
    time_limit = 3
    testfiles = []
    """for file in os.listdir(args.test_data):
        if file.endswith(".json"):
            testfiles.append(path.join(args.test_data, file))
    instances = createJSSPs(testfiles)

    print(algos)
    print("\n".join(testfiles))
    results = dict()
    times = dict()
    for algo in algos:
        results[algo], times[algo] = run_test_series(algo, test_instances=testfiles, time_limit=time_limit)
    data = []
    for num, test_file in enumerate(testfiles):
        row = dict()
        inst = instances[num]
        row["Instance"] = path.basename(test_file).replace(".json", "")
        row["size"] = len(instances[num].operations_by_id)
        row["real"] = row["Instance"] in real
        for algo in algos:
            schedule = results[algo][num]
            is_feasible = is_feasible_solution(inst, schedule)
            if is_feasible:
                row[algo] = round(objective_value(inst, schedule), 1)
            else:
                row[algo] = "FAIL"
            row[f"T_{algo}"] = round(times[algo][num], 1)
        data.append(row)
    df = pd.DataFrame(data)
    df.to_json(path.join("table", f"data{datetime.today()}.json"))
    print(df)"""
    df = pd.read_json("table/data2024-10-25 15:05:24.050463.json")
    data = df.to_dict("records")
    print(df)
    # round
    # add artificial column
    for row in data:
        if row["NaivMIP"] == "FAIL" and not row["BottleneckPD"] == "FAIL":
            row["NaivMIP"] = row["BottleneckPD"]
        if not row["NaivMIP"] == "FAIL" and not row["BottleneckPD"] == "FAIL":
            row["NaivMIP"] = min(row["BottleneckPD"], row["NaivMIP"])

        obj = [row[algo] for algo in algos if not row[algo] == "FAIL"]
        opt =  min(obj)
        row["real"] = "Yes" if row["real"] else "No"
        for algo in algos:
            if not row[algo] == "FAIL":
                row[algo] = f"{round((row[algo]/opt - 1)*100, 2)} \\,\\%"
        for key, val in row.items():
            if "T_" in key:
                row[key] = f"{round(max(val, 0.01), 2)}\\,s"
    df2 = pd.DataFrame(data)
    print(df2)
    # change to "time"
    # add midrules
    # scale large numbers
    # remove inst_name
    df2.to_latex(
        buf="algo_eval_table.tex",
        columns=["size", "real", "NaivMIP","T_NaivMIP", "BottleneckPD", "T_BottleneckPD", "CP-Solver", "T_CP-Solver", "LPTFPD", "T_LPTFPD"],
        header=["size", "real", "MIP", "time", "Our PDH", "time", "CP", "time", "epst PDH", "time"],
        column_format="l|l|l|rr|rr|rr|rr"
    )

