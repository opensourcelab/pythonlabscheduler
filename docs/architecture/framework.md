# Framework overview

The scheduler is designed stateless. Meaning no information of past solutions or problems
are saved. This brings the advantage, that a scheduler running as a mirco service
can be accessed by several clients, stopped, restarted and have its settings changed
without affecting running experiments using it.

## Accessing the scheduler
- better automize problem generation(see [example](../../tests/sila_call_test.py))

### Via python import
- link to JSSP in structures

### Via SiLA
# TODO: release
- recommended
- explain/link sila_python
- link to [example](../../tests/sila_call_test.py)
- explain wfg_str and sila_struct [example](../../tests/call_comparison_test.py)


## Algorithm plugin

By design, the scheduler provides to different scheduling algorithms.
There is a [solver_interface](../../pythonlabscheduler/solver_interface.py) defined
in python and every module placed in [this folder](../../pythonlabscheduler/solvers)
is automatically included.

### Choosing an algorithm
At any point in time the schedulers algorithm can be switched, e.g. via a SiLA command.
You can also request a list of available algorithms, request the basic information
on any available


[# TODO: release Insert exemplary ipython in-out correspondences]

### Shipped algorithms
Currently, there are the following scheduling algorithms implemented:

- [Simple Solver](../solvers/simple.md)
- [Basic Priority Dispatching Heuristic](../solvers/basic_pd_heur.md)
- [Advanced Priority Dispatching Heuristic](../solvers/specialized_pd_heur.md)
- [MIP Optimizer](../solvers/mip_solver.md)

A [Graph Neural Network-Priority Dispatching Heuristic]() is currently under construction.
The (also ongoing) theoretical work on the can be found [here]()
