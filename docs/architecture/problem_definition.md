# Problem Definition

`Scheduling` in this sense means
taking a workflow description and a descriptions of a lab and
assigning timestamps and machines to the steps of the workflow
in an intelligent way.

## Examples

We present a simple and a complex example for this. Both are scheduled in the same lab.
The lab needs to be defined my a yaml file like [this](../../pythonlabscheduler/sila_server_config_changed.yml).
The server credentials there are redundant. We just use the same yaml file to configure
multiple parts or our lab automation framework. The Important information is
name, type and capacities of devices.

### Example 1

[# TODO: for release]

Inc_Read_Process


### Example 2

[Marin_assay - multi-plate complex assay]


## Formal problem description

# Definition of the JSSP and our generalization

## Standard JSSP

We begin with the usual definition of the *Job Shop Scheduling Problem* (JSSP). The task is to assign an optimal schedule to a pair of workflow and lab.

**Standard JSSP**

A *JSSP* of size {math}`n \times m` with {math}`n, m \in \mathbb{N}` consists of a lab of {math}`m` machines {math}`M = M_1, \ldots, M_m` and a workflow of {math}`n` jobs {math}`J = J_1, \ldots, J_n` which each job consisting of {math}`m` operations {math}`J_j = o_{1j}, \ldots, o_{mj}`. Each operation has a duration {math}`d_{ij} > 0`. Each operation of one job is assigned to a different machine {math}`M_{ij}`, i.e. {math}`M_{kj} \neq M_{lj}` for {math}`k \neq l`.

This means that every machine will process {math}`n` operations. Any machine can only do one operation at a time. The operations of each job have to be done in order, but the order of assigned machines may differ between jobs.

**Schedule**

A *schedule* {math}`S = \left(t_{ij}\right)_{i=1, \ldots, m}^{j=1, \ldots, n} \in \mathbb{R}_+^{n \times m}` is valid for a standard JSSP if and only if

```{math}
\begin{align*}
    t_{ij} + d_{ij} \leq& t_{i+1,j} \text{ for } i=1, \ldots, n-1 \text{ and } j=1, \ldots, n \\
    t_{ij} + d_{ij} \leq& t_{kl} \text{ for } M_{ij}=M_{kl} \text{ and } t_{ij} < t_{kl}
\end{align*}
```

The makespan of a schedule {math}`S` is the finishing time of the last operation, i.e. {math}`\max_{ij} (t_{ij} + d_{ij})`. The optimal schedule in the standard setting is a valid schedule with minimal makespan.

## Generalization

### Generalization of workflows

For our workflows, we do not require the property that the operations are divided into independent jobs of equal length. We drop the concept of jobs and only consider a set of operations with any partial order on them:

**Workflow**

A *workflow* {math}`G` consists of a set of operations {math}`O = \{o_i\}_{i \in I}` and a partial order {math}`E \subset O \times O`, such that {math}`G := (O, E)` forms an acyclic, directed graph. There is a subset {math}`O_m \subseteq O`.

Moreover, our workflows can include soft and hard time constraints. We allow for {math}`G = (O, E)`

- Minimum waiting times {math}`e_{w\_min}` for {math}`e \in E`
- Maximum waiting times {math}`e_{w\_max}` for {math}`e \in E`
- Waiting costs {math}`e_{w\_c}` for {math}`e \in E`

Waiting costs between two operations mean that the scheduling algorithm tries to avoid waiting between them, but does not have to.

### Generalization of Machines

In the standard JSSP, machines are symmetric and can do one operation at a time. That does not meet the needs of most laboratories. Therefore, we allow a machine {math}`m` to have

- a maximum holding capacity {math}`m_c \in \mathbb{N}`
- a maximum processing capacity {math}`m_{p\_max} \in \mathbb{R}^+`
- a minimum processing capacity {math}`m_{p\_min} \in \mathbb{R}^+`
- a type of machine {math}`m_t`

For example, an incubator shaker can hold and shake multiple labware, a centrifuge can hold multiple labware but needs to be full to be balanced, and a pipetting robot can hold multiple plates but run only one protocol at a time. The type is useful because there might be several identical machines in a lab.

### Generalization of Schedules

Since there can be several machines of a kind, the scheduler not only has to decide when to start an operation but also which machine will execute it. Formally, we write {math}`o_t` for {math}`o \in O` for the type of machine that has to execute an operation. From this point on, we denote the duration of an operation by {math}`d(o)`.

**Generalized JSSP**

Together, an instance {math}`P = (G, M)` of our generalization of the JSSP consists of a workflow {math}`G = (O, E)` and a set of machines (lab) {math}`M`.

A solution *schedule* is an assignment of starting times and executing machines to all operations of a workflow without violating any of the waiting or capacity constraints. More formally:

**Generalized schedule**

In our sense, a valid *schedule* {math}`s` to a scheduling problem {math}`P = (G, M) = ((O, E), M)` is a function


```{math}
\begin{equation*}
    s:O \rightarrow \mathbb{R}^+ \times M: o \mapsto (t(o), m(o))
\end{equation*}
```

satisfying

```{math}
\begin{align}
    \label{ref:prec_con}t(o) + d(o) +e_{w\_min} \leq t(\tilde o) &\text{ for all } e=(o,\tilde o)\in E \\
    \label{ref:wait_con}t(o) + d(o) +e_{w\_max} \geq t(\tilde o) &\text{ for all } e=(o,\tilde o)\in E
\end{align}
```

The objective value of the schedule is

```{math}
\begin{equation*}
    V(s) = \text{makespan}(s) + \alpha \times \text{wait\_costs}(s)
\end{equation*}
```

with some factor {math}`\alpha \in \mathbb{R}` balancing makespan (total running time) and total costs for waiting between operations. Formally, we have

```{math}
\begin{align*}
    \text{makespan}(s) :=& \max_{o \in O} (t(o) + d(o)) \\
    \text{wait\_cost(s) :=}& \sum_{e = (o, \tilde o) \in E} t(\tilde o) - (t(o) + d(o))
\end{align*}
```

Equation (\ref{ref:prec_con}) formalizes the precedence constraints and minimum waiting times. Equation (\ref{ref:wait_con}) formalizes the maximum waiting times. Capacity and processing capacity can also be formalized but would be hardly readable. See documentation of the MIP-Solver for more information on that.

An optimal schedule is a schedule with minimal objective value. This is very hard to compute, and in laboratories, usually, a valid schedule is all we need. Even finding a valid schedule proves challenging for complex workflows.


[link to generalized_JSSP.pdf]()

