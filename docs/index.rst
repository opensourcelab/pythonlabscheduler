Welcome to Python Lab Scheduler's documentation!
======================================================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   architecture/*
   solvers/*
   source/modules
   development
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
