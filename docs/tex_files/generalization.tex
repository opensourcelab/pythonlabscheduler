\documentclass[a4paper, 12pt]{article}
\input{imports}
\input{commands}
\usepackage{tabularx}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{geometry}
\begin{document}
\newgeometry{left= 1.5cm, bottom= 2cm, top= 3cm, right= 1.5cm}
\title{Definition of the JSSP and our generalization}
\date{}
\author{Stefan Maak}
\maketitle

\section{Standard JSSP}
We begin with the usual definition of the \emph{Job Shop Scheduling Problem}(JSSP).
The task is to assign an optimal schedule to a pair of workflow and lab.
\begin{definition}[Standard JSSP]
	A \emph{JSSP} of size $n\times m$ with $n,m\in\MN$ consists of a lab of m machines $M=M_1,..,M_m$ and a workflow of $n$ jobs $J=J_1,..,J_n$
	which each job consisting of $m$ operations $J_j=o_{1j},..,o_{mj}$.
	Each operation has a duration $d_{ij}>0$.
	Each operation of one job is assigned to a different machine $M_{ij}$, i.e. $M_{kj}\neq M_{lj}$ for $k\neq l$.
\end{definition}
This means that every machine will process $n$ operations. Any machine can only do one operation at a time.
The operations of each job have to be done in order, but the order of assigned machines may differ between jobs.

A schedule to such a JSSP is an assignment of starting times to each operations, such that these conditions are fulfilled. More precisely:
\begin{definition}[Schedule]
	A \emph{schedule} $S=\left(t_{ij}\right)_{i=1,..,m}^{j=1,..,n}\in \MR_+^{n\times m}$ is valid for a standard JSSP if and only if
	\begin{align*}
		t_{ij}+d_{ij}\leq& t_{i+1,j}\text{ for }i=1,..,n-1 \text{ and } j=1,..,n\\
		t_{ij}+d_{ij}\leq& t_{kl} \text{ for } M_{ij}=M_{kl} \text{ and } t_{ij}<t_{kl} \\
	\end{align*}
\end{definition}

The makespan of a schedule $S$ is the finishing time of the last operation. i.e. $\underset{ij}{\max}\,(t_{ij}+d_{ij})$.
The optimal schedule in the standard setting is a valid schedule with minimal makespan.

\section{Generalization}
\subsection{Generalization of workflows}
	For our workflows we do not require property that the operations are divided into independent jobs of equal length.
	We drop the concept of jobs and only consider a set of operations with any partial order on them:
	\begin{definition}[Workflow]
		A \emph{workflow} $G$ consists of a set of operations $O=\{o_i\}_{i\in I}$ and a partial order $E\subset O\times O$,
		such that $G:=(O,E)$ forms an acyclic, directed graph. There is a subset $O_m\subseteq O$.
	\end{definition}
	We call the partial order \emph edges. The subset $O_m$ indicates the moving operations in a robotic lab.\\
	Moreover our workflows can include soft and hard time constraints. We allow for $G=(O,E)$
	\begin{itemize}
	  \item - Minimum waiting times $e_{w\_min}$ for $e\in E$
	  \item - Maximum waiting times $e_{w\_max}$ for $e\in E$
	  \item - Waiting costs $e_{w\_c}$ for $e\in E$
	\end{itemize}
	Waiting costs between two operations mean, that the scheduling algorithm tries to avoid waiting between them, but does not have to.
\subsection{Generalization of Machines}
	In the standard JSSP, machines are symmetric and can do one operation at a time. That does not meet the needs of most laboratories.
	Therefore we allow a machine $m$ to have
	\begin{itemize}
	  \item a maximum holding capacity $m_c\in\MN$
	  \item a maximum processing capacity $m_{p_max}\in\MR^+$
	  \item a minimum processing capacity $m_{p_min}\in\MR^+$
	  \item a type of machine $m_t$
	\end{itemize}
	For example, an incubator shaker can hold and shake multiple labware,
	a centrifuge can hold multiple labware but needs to be full to be balanced
	and a pipetting robot can hold multiple plates but run only one protocol at a time.
	The type is useful because there might be several identical machines in a lab.
\subsection{Generalization of Schedules}
	Since there can be several machines of a kind, the scheduler not only has decide when to start an operations but also which machine will execute it.
	Formally, we write $o_t$ for $o\in O$ for the type of machine, that has to execute an operations. From this point on we denote the duration of an operation
	by $d(o)$.
	\begin{definition}[Generalized JSSP]
		Together, an instance $P=(G,M)$ of our generalization of the JSSP consists of a workflow $G=(O,E)$ and a set of machines (lab) $M$.
	\end{definition}

	A solution \emph{schedule} is an assignment of starting times and executing machines to all operations of a workflow without violating
	any of the waiting or capacity constraints.
	More formally:
	\begin{definition}[Generalized schedule]
		In our sense, a valid \emph{schedule} $s$ to a scheduling problem \\
		$P=(G,M)=((O,E),M)$ is a function
		\begin{equation*}
			s:O\rightarrow \MR^+\times M: o\mapsto (t(o), m(o))
		\end{equation*}
		satisfying
		\begin{align}
			\label{ref:prec_con}t(o) + d(o) +e_{w\_min} \leq t(\tilde o) &\text{ for all } e=(o,\tilde o)\in E  \\
			\label{ref:wait_con}t(o) + d(o) +e_{w\_max} \geq t(\tilde o) &\text{ for all } e=(o,\tilde o)\in E  \\
			\text{holding capacity constraints}\\
			\text{processing capacity constraints}
		\end{align}
		The objective value os the schedule is
		\begin{equation*}
			V(s) = makespan(s) + \alpha*wait\_costs(s)
		\end{equation*}
		with some factor $\alpha\in\MR$ balancing makespan(total running time) and total costs for waiting between operations.
		Formally, we have
		\begin{align*}
			makespan(s) :=& \underset{o\in O}{\max}\,t(o) + d(o)\\
			wait\_cost(s) :=& \underset{e=(o,\tilde o)\in E}\sum t(\tilde o)-(t(o)+d(o))
		\end{align*}
	\end{definition}
	Equation (\ref{ref:prec_con}) formalizes the precedence constraints and minimum waiting times.
	Equation (\ref{ref:wait_con}) formalozes the maximum waiting times.
	Capacity and processing capacity can also be formalized, but would be hardly readable. See dokumentation of the MIP-Solver for more information on that.
	\par
	An optimal schedule is a schedule with minimal objective value.
	This is very hard to compute and in laboratories, usually a valid schedule is all we need.
	Even finding a valid schedule proves challenging for complex workflows.


\end{document}
