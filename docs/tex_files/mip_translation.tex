\documentclass[a4paper, 12pt]{article}
\input{imports}
\input{commands}
\usepackage{tabularx}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{geometry}
\begin{document}
\newgeometry{left= 1.5cm, bottom= 2cm, top= 3cm, right= 1.5cm}
\title{Translation of a set of jobs to a MIP}
\date{}
\author{Stefan Maak}
\maketitle

\section{Characterization of jobs and laboratory}
The lab environment is characterized by the participating devices. There are different classes of devices and some classes might have multiple instances participating. For example, the Greifswald laboratory has three incubators and two plate readers. Then there are also capacities limiting the number of containers a device can hold or process at a time.
\par
From the workflow graph of a process(or multiple ones), we parse a set of jobs. Examples for jobs are incubating a microplate or moving it from one site to another. All necessary information on the process(-es) should be contained in the properties of the jobs. Every job has the following properties:
\begin{itemize}
	\item A set of jobs that have to be finished before it can start.
	\item A set of jobs that have to be started before it can start. This might be useful to preserve certain orders.
	\item A set of participating devices. For example, in the movement of a plate from one device to another, three devices participate (origin, destination and moving-robot). So far, we do not allow jobs to use multiple devices of the same class.
	\item Information on shared devices with prior jobs. Imagine, there is more than one incubator involved. Then the incubator a plate is incubated in should be the same it was moved to.
	\item Maximum waiting times between starting this job and finishing the prior ones.
	\item An estimated duration of the job.
	\item Costs for waiting time between starting this job and finishing prior ones. There can be different costs for different prior jobs.
\end{itemize}
A scheduling maps starting times to each job as well as instances to each of the jobs participating device classes. In the following sections, we formalize finding the best scheduling into a MIP(\emph{Mixed Integer Program}).

\section{A mathematical formulation of the scheduling problem}
For easier reading, we use capital letters for sets and device-related variables. Let $D=\left\{D_k\mid k=1,..,m\right\}$ be the set of device classes. In a lab, there are possibly multiple or no instances of a class. So let $A_k$ be the index set for the devices of class $D_k$ present in the lab. We denote the capacity of device class $D_k$ by $C_k\in\MR$. Maybe it will be necessary to model different instances of a device class with different capacities but for now, we skip that to avoid complexity.
\par
Suppose, we have $n$ jobs $J=\{j_i\mid i=1,..,n\}$. For each job $j_i$, we define
\begin{itemize}
	\item $f_i\subset J$ as the set of jobs that have to be finished before starting job $j_i$,
	\item $b_i\subset J$ as the set of jobs that have to begin before starting job $j_i$,
	\item $p_i\subseteq D$ as the set of device classes that participate in job $j_i$,
	\item $s_i:f_i\mapsto \mathcal{P}(p_i)$, $\mathcal{P}$ denoting the power set, as the mapping that yields the set of device classes for which $j_i$ and a prior job $j\in f_i$ must use common instances,
	\item $w_i:f_i\mapsto\MR$ as the function yielding the maximum waiting time after a job $j\in f_i$ is finished (can be infinity),
	\item $d_i\in\MR$ as the duration of job $j_i$,
	\item $c_i:f_i\mapsto\MR$ as the function yielding the waiting costs per time unit time after a job $j\in f_i$ is finished.
\end{itemize}
A scheduling of these jobs consists of orders when to start and which devices to use for each job. More formally:
\begin{definition}
	A \emph{scheduling} is defined as a pair $(x,y)$, where $x\in\MR_+^n$ denotes the starting times and $y:(j_i, D_k)\rightarrow A_{kl}$ denotes a mapping, which maps a job $j_i$ and a device class $D_k\in p_i$ to an instance $A_{kl}\in A_k$ of that class.
\end{definition}

The objective is to finish the complete process as soon as possible and reduce costs (for waiting and possibly others). The finishing time of a scheduling $(x,y)$ is
\begin{equation}
	T(x) = \mmax{j_i\in J} (x_i+d_i) \,\,.
\end{equation}

The waiting costs of a scheduling $(x,y)$ are
\begin{equation}
	W(x) = \Su{j_i\in J}{} \Su{j_o\in f_i}{}c_i(j_o)\cdot (x_i-x_o-d_o) \,\,.
\end{equation}
Summing and weighting these with a free to choose factor $\alpha\in\MR$ we can write the objective as
\begin{equation}
	F(x)= \alpha T(x)+W(x) \,\,. \label{eq:objective}
\end{equation}
Next, we have to formulate the constraints for a scheduling to be valid. Enforcing the correct order of starting and finishing jobs is done by
\begin{equation}
	x_o+d_o\leq x_i \forall j_o\in f_i , j_i\in J\label{eq:prior_finished}
\end{equation}
and
\begin{equation}
	x_o\leq x_i \forall j_o\in b_i , j_i\in J\,.\label{eq:prior_started}
\end{equation}
To enforce the maximum waiting times we demand
\begin{equation}
\label{eq:max_wait}
x_o+d_o+w_i(j_o)\geq x_i \forall j_o\in f_i , j_i\in J\,.
\end{equation}
For all cases without a maximum waiting time, this inequality can be omitted. Next, we enforce the sharing of devices with
\begin{equation}
\label{eq:shared}
y(j_i, D_k)=y(j_o, D_k) \forall D_k\in s_i(j_o), j_o\in f_i , j_i \in J\,.
\end{equation}
These were all constraints that have to be met for a valid workflow. Now, we have to consider the limits of the lab. Note, that the duration of jobs might already depend on the lab. But mainly, we have to ensure the maximum capacities of devices are not exceeded. In words, we must demand
\begin{equation*}
\begin{array}{c}
\text{''At no time, the number of running jobs needing such a device} \\
\text{and using this particular instance must exceed its capacity''}
\end{array}
\end{equation*}
As a formula, this writes
\begin{equation}
\label{eq:capacity_hard}
\left|\left\{j_i\in J\mid D_k\in p_i,y(j_i, D_k)=A_{kl}, x_i\leq t\leq x_i+d_i\right\}\right|\leq C_k \forall A_{kl}\in A_k, D_k\in D , t\in[0,T(x)]
\end{equation}
All prior formulas were more or less suitable for a MIP, but this ugly one needs some reformulation.
\eqref{eq:capacity_hard} reduces when there is only one instance of a device class or its capacity equals 1 or less jobs are using this device class than the capacity (f.e. a cytomat can hold 32 containers). Exploiting such cases reduces the problem complexity significantly.
\section{Formulating the MIP}
First, we break down the general case of \eqref{eq:capacity_hard}. For this, we temporary fix the jobs using a particular device, meaning a particular instance $A_{kl}$ of a particular device class $D_k$:
\begin{equation*}
	\bar J=\{j_i\in J\mid D_k\in p_i,y(j_i, D_k)=A_{kl}\}
\end{equation*}
Further, we introduce binary variables $s_{oi}\in\{0,1\}$ and $f_{oi}\in\{0,1\}$ for $j_o,j_i\in \bar J$ which shall indicate
\begin{equation*}
\begin{array}{rll}
	s_{oi}=1\Leftrightarrow & x_o\leq x_i &\text{ , i.e. job $j_o$ started before job $j_i$}\\
	f_{oi}=1\Leftrightarrow & x_o + d_o < x_i&\text{ , i.e. job $j_o$ finished before job $j_i$ is started}
\end{array}
\end{equation*}
With these variables, we can handle the capacities by demanding
\begin{equation}
	\label{eq:capacity_easy}
	\Su{j_o\in\bar J}{}s_{oi}-f_{oi}\leq C_k-1 \forall j_i\in\bar J\,.
\end{equation}
Then, we add
\begin{equation}
	\begin{array}{c}
		x_i\leq x_o + s_{oi}M\\
		x_o\leq x_i + (1-s_{oi})M\\
		s_{oi}+s_{io} = 1
	\end{array}
\end{equation}
, where $M$ is some huge number (f.e. upper bound of $T(x)$), to ensure that $s_{oi}$ is chosen correctly.
Adding the third equation avoids the corner case where $x_i=x_o$ and $s_{io}=s_{oi}=0$. Similar, we handle $f_{oi}$ by adding
\begin{equation}
	\begin{array}{c}
		x_i\leq x_o +d_o + f_{oi}M\\
		x_o + d_o\leq x_i + (1-f_{oi})M\\
	\end{array}
\end{equation}

Putting everything together, we can formulate our optimization problem:
\begin{equation}
\optiprobl{T + \Su{j_i\in J}{} \Su{j_o\in f_i}{}c_i(j_o)\cdot (x_i-x_o+d_o)}
{
x_i+d_i\leq&T&\forall j_i\in J\\
&x_o+d_o\leq& x_i &\forall j_o\in f_i , j_i\in J,\\
&x_o+d_o+w_i(j_o)\geq& x_i &\forall j_o\in f_i , j_i\in J,\\
&y_{ikl}=&y_{okl}&\forall D_k\in s_i(j_o), j_o\in f_i , j_i \in J, A_{kl}\in A_k,\\
& \Su{A_{kl}\in A_k}{}y_{ikl}=&1&\forall j_i\in J, D_k\in p_i,\\
&\Su{\tiny\begin{array}{c}j_o\in\bar J\\D_k\in p_o\end{array}}{}
(s_{oi}-f_{oi})\cdot y_{okl}\textbf{\textcolor{red}{!!!}}\leq& (1-y_{ikl})M+C_k-1 &\forall j_i\in J, D_k\in p_i, A_{kl}\in A_k,\\
&x_i + \varepsilon\leq& x_o + s_{oi}M & \forall j_i,j_o\in J,\\
&x_o + \varepsilon\leq& x_i + (1-s_{oi})M & \forall j_i,j_o\in J,\\
&x_i + \varepsilon\leq& x_o + d_o + f_{oi}M & \forall j_i,j_o\in J,\\
&x_o + d_o + \varepsilon\leq& x_i + (1-f_{oi})M & \forall j_i,j_o\in J,\\
&x_i \geq &0 &\forall j_i\in J\\
&y_{ikl} \in& \{0,1\} & \forall A_{kl}\in A_k , D_k\in p_i , j_i\in J,\\
&s_{oi} \in & \{0,1\} & \forall j_i,j_o\in J,\\
&f_{oi} \in & \{0,1\} & \forall j_i,j_o\in J,\\
&0\leq& T,
}
\end{equation}
where the binary variables $y_{ikl}$ indicate whether job $j_i$ uses device $A_{kl}$. They model the mapping $y$ of the scheduling.
The \textbf{\textcolor{red}{!!!}} mark a quadratic term. In
\begin{equation}
\label{eq:capacity_quad}
\Su{\tiny\begin{array}{c}j_o\in\bar J\\D_k\in p_o\end{array}}{}
(s_{oi}-f_{oi})\cdot y_{okl}\leq (1-y_{ikl})M+C_k-1\,,
\end{equation}
the term $(1-y_{ikl})M$ makes the inequality always valid if job $j_i$ does not use device $A_{kl}$. Multiplying each summand on the left-hand side by $y_{okl}$ filters the jobs not using $A_{kl}$. To get rid of the quadratic term we have to introduce more variables using the following trick.
\begin{lemma}
	Let $a,b\in \{0,1\}$. Then it holds
	\begin{equation}
		h=a\cdot b\Leftrightarrow \left\{\begin{array}{lr}
			h\in&\{0,1\}\\
			a+b\leq& 1+h\\
			h\leq&a\\
			h\leq&b
		\end{array}\right.
	\end{equation}
\end{lemma}
Thereby, we can finally formulate our problem as MIP:
\begin{equation}
\optiprobl{T + \Su{j_i\in J}{} \Su{j_o\in f_i}{}c_i(j_o)\cdot (x_i-x_o+d_o)}
{
x_i+d_i\leq&T&\text{ for all } j_i\in J\\
&x_o+d_o\leq& x_i &\text{ for all } j_o\in f_i , j_i\in J,\\
&x_o+d_o+w_i(j_o)\geq& x_i &\text{ for all } j_o\in f_i , j_i\in J,\\
&y_{ikl}=&y_{okl}&\text{ for all } D_k\in s_i(j_o), j_o\in f_i , j_i \in J, A_{kl}\in A_k,\\
& \Su{A_{kl}\in A_k}{}y_{ikl}=&1&\text{ for all }j_i\in J, D_k\in p_i,\\
&\Su{\tiny\begin{array}{c}j_o\neq j_i\in\bar J\\D_k\in p_o\end{array}}{}h_{oikl}
\leq& (1-y_{ikl})M+C_k-1 &\text{ for all } j_i\in J\ , D_k\in p_i , A_{kl}\in A_k,\\
&s_{oi}-f_{oi}+y_{okl}\leq& h_{oikl}+1&\text{ for all } j_o\neq j_i\in J\ , D_k\in p_i\cap p_o , A_{kl}\in A_k, \\
&h_{oikl}\leq&s_{oi}-f_{oi}&\text{ for all } j_o\neq j_i\in J\ , D_k\in p_i\cap p_o , A_{kl}\in A_k,\\
&h_{oikl}\leq&y_{okl}&\text{ for all } j_o\neq j_i\in J\ , D_k\in p_i\cap p_o , A_{kl}\in A_k,\\
&x_i + \varepsilon\leq& x_o + s_{oi}M & \text{ for all } j_o\neq j_i\in J,\\
&x_o + \varepsilon\leq& x_i + (1-s_{oi})M & \text{ for all } j_o\neq j_i\in J,\\
&x_i + \varepsilon\leq& x_o + d_o + f_{oi}M & \text{ for all } j_o\neq j_i\in J,\\
&x_o + d_o + \varepsilon \leq& x_i + (1-f_{oi})M & \text{ for all } j_o\neq j_i\in J,\\
&x_i \geq &0 &\text{ for all } j_i\in J\\
&y_{ikl} \in& \{0,1\} & \text{ for all } A_{kl}\in A_k \forall D_k\in p_i \forall j_i\in J,\\
&s_{oi} \in & \{0,1\} & \text{ for all } j_i,j_o\in J,\\
&f_{oi} \in & \{0,1\} & \text{ for all } j_i,j_o\in J,\\
&h_{oikl} \in & \{0,1\} & \text{ for all } j_i\in J\ , D_k\in p_i\cap p_o , A_{kl}\in A_k\,\\
&0\leq& T.
}
\end{equation}
In this formulation a variable $h_{oikl}$ indicates whether jobs $j_i,j_o\in J$ use device $A_{kl}$ of device class $D_k\in D$ at a common point in time.
\section{Discussion of the MIP model}
\subsection{Strengths}
The following aspects can be modelled with our MIP but are not included in the standard job-shop-problem.
\begin{itemize}
	\item Jobs can be processed by the same machine more than once.
	\item Machines can process more than one Job at a time. This can be limited to a certain capacity.
	\item Maximum waiting times and waiting costs are modelled (Also this is not difficult).
\end{itemize}
\subsection{Weaknesses}
The following aspects are missing or at least nice to have for our application.
\begin{itemize}
	\item Jobs should possibly be marked as done or get assigned prefixed starting times (should not be difficult). This is neccessary for online adaptive rescheduling.
	\item There are no optional jobs. This might be useful for heating/cooling or adding dummy-plates into the centrifuge for balance.
	\item Devices have no changeable(f.e. by optional jobs) status. This could be the temperature of a cytomat or the source-material linked to the dispenser.
	\item The model has many binary variables. In theory the number of $s_{oi}$ and $f_{oi}$ is in $O(n^2)$ and the number of $h_{oikl}$ is in $O(n^2m\cdot \mmax{k}|A_k|)$. In practice they they are needed sparsely and in addition it is easy to preprocess on them. F.e. $s_{oi}\wedge s_{ip}\Rightarrow s_{op}$. We have yet to test how many variables there will be in typical examples.
\end{itemize}
\end{document}
