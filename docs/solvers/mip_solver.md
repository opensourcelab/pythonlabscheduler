# Mixed Integer Program Solver



A Mixed Integer Program (MIP) solver is a type of optimization software that is used to solve complex mathematical problems. MIP solvers are particularly useful in job shop scheduling problems, which involve scheduling a set of jobs on a set of machines subject to various constraints. In a job shop scheduling problem, each job consists of a set of operations that must be performed on a set of machines in a specific order. The goal is to minimize the total time required to complete all jobs while satisfying various constraints.

MIP solvers work by formulating the job shop scheduling problem as a mathematical model that can be solved using linear programming techniques. The model consists of a set of decision variables, constraints, and an objective function. The decision variables represent the start times of each operation on each machine, and the objective function represents the total time required to complete all jobs. The constraints represent various constraints such as machine capacities, precedence constraints, and resource constraints.

In the current implementation of the job shop scheduling problem, machine capacities have been added as a constraint. This means that each machine has a limited capacity, and the total processing time of all jobs assigned to a machine cannot exceed its capacity. This constraint can be incorporated into the mathematical model by adding additional constraints that limit the total processing time of jobs assigned to each machine.

MIP solvers can be applied to solve job shop scheduling problems by formulating the problem as a mathematical model and then using the solver to find an optimal solution. The solver works by iteratively solving a series of linear programming problems, each of which is a relaxation of the original problem. The solution to each relaxation provides a lower bound on the optimal solution, and the solver uses this information to guide the search for the optimal solution.

In summary, MIP solvers are powerful optimization tools that can be used to solve complex job shop scheduling problems. The current implementation of the job shop scheduling problem extends the problem by adding machine capacities as a constraint. This constraint can be incorporated into the mathematical model used by the MIP solver to find an optimal solution. By using MIP solvers, it is possible to find optimal solutions to job shop scheduling problems that are difficult or impossible to solve using other methods.

(gitlab copilot)



[Link to MIP_Formulation.pdf]()


Mixed-Integer Programming (MIP) solvers are optimization tools that can be used to solve complex scheduling problems, including variations of the Job Shop Problem (JSP), such as the lab automation job shop problem. MIP solvers formulate the scheduling problem as a mathematical model, which is then solved to find an optimal or near-optimal solution. Here's how you can use a MIP solver for the lab automation job shop problem:

### Formulating the Problem:

1. **Decision Variables:**
   - Identify decision variables that represent the scheduling decisions. In the context of lab automation, these might include variables indicating the start time of each task on each resource (robotic arm, instrument).

2. **Objective Function:**
   - Define an objective function that you want to optimize. It could be minimizing the makespan (total time to complete all tasks), maximizing resource utilization, or meeting specific project deadlines.

3. **Constraints:**
   - Formulate constraints that reflect the characteristics of the lab automation system. Constraints may include task dependencies, resource availability, and any specific requirements of the experiments.

### Example Formulation (Pseudo-Code):

```python
# Assuming a set of tasks T, resources R, and a binary variable x[i, j] indicating if task i starts on resource j.

# Decision Variables
x[i, j]  # Binary variable indicating if task i starts on resource j

# Objective Function (Minimize Makespan)
minimize sum(x[i, j] * completion_time[i] for i in tasks for j in resources)

# Constraints
for i in tasks:
    # Each task must start exactly once
    sum(x[i, j] for j in resources) == 1

for j in resources:
    # Each resource can only handle one task at a time
    sum(x[i, j] for i in tasks) <= 1

for i in tasks:
    for j in resources:
        # Task dependencies
        start_time[i] >= completion_time[p] + processing_time[p, j] for p in dependencies[i]

# Additional constraints based on resource availability, project deadlines, etc.
```

### Using a MIP Solver:

1. **Choose a MIP Solver:**
   - Popular MIP solvers include IBM CPLEX, Gurobi, and SCIP. Choose a solver based on your requirements and available resources.

2. **Install and Configure the Solver:**
   - Install the chosen MIP solver and configure it for your programming environment (e.g., Python, Java, MATLAB).

3. **Implement the Formulated Model:**
   - Use the solver's programming interface to implement the formulated MIP model. This involves defining decision variables, objective function, and constraints.

4. **Solve the Model:**
   - Call the solver to solve the model. The solver will use optimization algorithms to find a solution that minimizes or maximizes the objective function while satisfying the constraints.

5. **Retrieve and Analyze Results:**
   - Once the solver completes, retrieve the optimal or near-optimal solution. Analyze the results to understand the scheduling decisions, resource allocations, and overall performance.

### Example Code (using Python and Gurobi):

```python
from gurobipy import Model, GRB

# Create a new model
model = Model("lab_automation_schedule")

# Define decision variables
x = {}
for i in tasks:
    for j in resources:
        x[i, j] = model.addVar(vtype=GRB.BINARY, name=f"x_{i}_{j}")

# Set objective function
model.setObjective(sum(completion_time[i] * x[i, j] for i in tasks for j in resources), GRB.MINIMIZE)

# Add constraints
# (Assuming constraints are added based on the pseudo-code above)

# Optimize the model
model.optimize()

# Retrieve results
if model.status == GRB.OPTIMAL:
    print("Optimal solution found.")
    # Retrieve and analyze the solution
    for i in tasks:
        for j in resources:
            if x[i, j].x > 0.5:
                print(f"Task {i} starts on Resource {j} at time {start_time[i].x}")

# Close the model
model.close()
```

This is a simple example, and the actual implementation would depend on the specific characteristics and requirements of your lab automation scheduling problem. It's recommended to consult the documentation of the chosen MIP solver for more detailed guidance on model formulation and usage in your specific programming environment.

SCIP (Solving Constraint Integer Programs) is a powerful optimization solver that can be used for Mixed-Integer Programming (MIP) problems. Below is an example of how you can use SCIP to solve a simplified lab automation scheduling problem. Note that SCIP typically involves using a modeling framework like PySCIPOpt in Python.

```python
from pyscipopt import Model, quicksum

# Define tasks, resources, completion times, and task dependencies
tasks = [1, 2, 3]
resources = [1, 2]
completion_time = {1: 5, 2: 7, 3: 3}
dependencies = {2: [1], 3: [2]}

# Create SCIP model
model = Model("lab_automation_schedule")

# Define binary decision variables x[i, j]
x = {}
for i in tasks:
    for j in resources:
        x[i, j] = model.addVar(vtype="B", name=f"x_{i}_{j}")

# Set objective function (minimize makespan)
model.setObjective(quicksum(completion_time[i] * x[i, j] for i in tasks for j in resources), "minimize")

# Add constraints
for i in tasks:
    model.addCons(quicksum(x[i, j] for j in resources) == 1, name=f"task_{i}_start_once")

for j in resources:
    model.addCons(quicksum(x[i, j] for i in tasks) <= 1, name=f"resource_{j}_exclusive")

for i in tasks:
    for p in dependencies.get(i, []):
        model.addCons(x[i, j] * completion_time[i] >= x[p, j] * completion_time[p] + completion_time[p], 
                      name=f"dependency_{i}_{p}")

# Solve the model
model.optimize()

# Retrieve and print results
if model.getStatus() == "optimal":
    print("Optimal solution found.")
    for i in tasks:
        for j in resources:
            if model.getVal(x[i, j]) > 0.5:
                print(f"Task {i} starts on Resource {j} at time {model.getVal(completion_time[i])}")
else:
    print("No optimal solution found.")

# Close the model
model.freeProb()
```

In this example, we use the PySCIPOpt Python interface for SCIP. Note that you need to install PySCIPOpt and SCIP to run this example. The SCIP Python interface provides similar functionalities to other optimization solvers, allowing you to define decision variables, set the objective function, add constraints, and then optimize the model.

Please adjust the code based on the specifics of your lab automation scheduling problem, considering factors like task priorities, dynamic adjustments, and additional constraints based on your requirements.

In lab automation, there are several common constraints related to instruments such as incubators, plate readers, robotic arms, and plate hotels. Below are examples of some common constraints along with corresponding pseudo code for defining these constraints in SCIP using the PySCIPOpt modeling framework.

### Example Constraints:

#### 1. **Resource Allocation Constraint:**
   - Ensure that each task is assigned to only one resource.

   ```python
   for i in tasks:
       model.addCons(quicksum(x[i, j] for j in resources) == 1, name=f"task_{i}_allocation")
   ```

#### 2. **Exclusive Resource Use Constraint:**
   - Ensure that each resource is used by at most one task at any given time.

   ```python
   for j in resources:
       model.addCons(quicksum(x[i, j] for i in tasks) <= 1, name=f"resource_{j}_exclusive")
   ```

#### 3. **Task Dependency Constraint:**
   - Ensure that a task cannot start until its dependent tasks have completed.

   ```python
   for i in tasks:
       for p in dependencies.get(i, []):
           model.addCons(x[i, j] * completion_time[i] >= x[p, j] * completion_time[p] + completion_time[p], 
                         name=f"dependency_{i}_{p}")
   ```

#### 4. **Resource Availability Constraint:**
   - Ensure that a resource is not used before a certain time.

   ```python
   for j in resources:
       model.addCons(quicksum(x[i, j] * completion_time[i] for i in tasks) >= resource_availability_time[j], 
                     name=f"resource_{j}_availability")
   ```

#### 5. **Plate Hotel Capacity Constraint:**
   - Limit the number of tasks that can be placed in a plate hotel simultaneously.

   ```python
   for j in plate_hotels:
       model.addCons(quicksum(x[i, j] for i in tasks) <= plate_hotel_capacity[j], 
                     name=f"plate_hotel_{j}_capacity")
   ```

#### 6. **Incubation Time Constraint:**
   - Ensure that tasks requiring incubation satisfy a minimum incubation time.

   ```python
   for i in tasks_requiring_incubation:
       model.addCons(start_time[i] + incubation_time[i] <= completion_time[i], 
                     name=f"incubation_time_{i}")
   ```

### Note:
- In these pseudo code examples, `tasks`, `resources`, `dependencies`, `resource_availability_time`, `plate_hotels`, `plate_hotel_capacity`, `tasks_requiring_incubation`, `start_time`, `completion_time`, and `incubation_time` are assumed to be defined appropriately based on your specific problem.

- Constraints may need to be adjusted based on the characteristics of your lab automation system and the specific requirements of your scheduling problem.

- These are simplified examples, and in a real-world scenario, constraints may become more complex depending on the details of the lab automation processes.

Remember to adapt the pseudo code to the specifics of your lab automation scheduling problem and the data structures and parameters used in your actual implementation.