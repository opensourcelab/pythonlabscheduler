# Simple Solver

Round robin scheduling is a simple scheduling algorithm that assigns each operation to the next available machine. The algorithm works by iterating through the operations in the workflow and assigning each operation to the next available machine. If no machine is available, the operation is assigned to the next available machine after the current time. The algorithm continues until all operations have been assigned to a machine.

All steps are executed sequentially.

- Deterministic, no parallelization (!).
no device capacity is considered.
Danger !!! - only for demo purposes do not use in production since crashes could be possible !!! 



The algorithm is simple and easy to implement, but it does not take into account the processing time of each operation or the availability of machines. It also does not consider the order in which operations should be performed. For example, if an operation has a long processing time, it may be assigned to a machine that is not available for a long time. This can lead to delays in the schedule.



A simple solver for the Job Shop Problem is a basic algorithm that uses a brute-force approach to find a feasible solution. The algorithm works by generating all possible permutations of the operations for each job and then evaluating each permutation to determine if it satisfies the constraints of the problem. The algorithm continues to generate and evaluate permutations until a feasible solution is found.

The algorithm starts by generating all possible permutations of the operations for the first job. For each permutation, the algorithm evaluates the feasibility of the permutation by checking if the operations can be scheduled on the machines without violating any constraints. If the permutation is feasible, the algorithm generates all possible permutations of the operations for the second job and evaluates each permutation in combination with the first job. This process continues until all jobs have been scheduled.

The algorithm evaluates each permutation by checking if it satisfies the constraints of the problem. The constraints include precedence constraints, which require that certain operations be performed before others, and resource constraints, which limit the number of operations that can be performed on a machine at any given time. If a permutation violates any of the constraints, it is discarded and the algorithm moves on to the next permutation.

Once a feasible solution is found, the algorithm terminates and returns the solution. The solution consists of a schedule that specifies the start time of each operation on each machine. The schedule satisfies all constraints and minimizes the total time required to complete all jobs.

While a simple solver can be effective for small instances of the Job Shop Problem, it quickly becomes impractical for larger instances due to the large number of permutations that must be evaluated. In practice, more sophisticated algorithms such as Mixed Integer Programming (MIP) solvers or Constraint Programming (CP) solvers are used to solve the problem efficiently.

(github Copilot)