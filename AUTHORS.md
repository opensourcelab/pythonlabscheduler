
# Acknowledgements and Credits

The Python Lab Scheduler project thanks


Contributors
------------

* Stefan Maak <stefan.maak@uni-greifswald>

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* Stefan Maak <stefan.maak@uni-greifswald>
* mark doerr <mark.doerr@uni-greifswald.de>