"""
Tests configuring the lab and injecting solvers
"""

from pythonlabscheduler.scheduler_implementation import Scheduler
from os import path
from pythonlabscheduler.utility import parse_jobshop_from_yaml_file


def test_creation():
    scheduler = Scheduler()
    yaml_filename = path.join(path.dirname(__file__), 'test_data', 'lab_config_example.yaml')
    with open(yaml_filename, "r") as reader:
        yaml_file = reader.read()
        job_shop = parse_jobshop_from_yaml_file(yaml_file)
        scheduler.configure_job_shop(machine_list=job_shop)


if __name__ == "__main__":
    test_creation()
