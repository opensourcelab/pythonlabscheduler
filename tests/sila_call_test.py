"""
Starts a pythonlabscheduler server and tries to establish a connection.
Sends a scheduling task to the server and assumes a reasonable solution
"""
import time

import pytest
from pythonlabscheduler.sila_server import Server as SchedulerServer
from pythonlabscheduler.sila_server import Client as SchedulerClient
from os import path

@pytest.fixture
def server(scope='session'):
    server = SchedulerServer()
    server.start_insecure('127.0.0.1', 50071)
    yield server
    server.stop()


def test_call(server):
    """
    Incubate    Move    Pipett
      [10]  ->  [5] ->\
                       -> [10]
      [10]  ->  [5] ->/
    :return:
    """
    
    client = SchedulerClient.discover(server_name="Scheduler", insecure=True, timeout=5)
    lab_config_file = path.join(path.abspath(path.dirname(__file__)), 'test_data', "lab_config_example.yaml")
    with open(lab_config_file, 'r') as reader:
        client.LabConfigurationController.LoadJobShopFromFile(reader.read())

    steps = [
        # idx, duration, required_resources, start, finish, wait_to_start_cost
        ["S1", 10, [['IncubatorServiceResource', 'main', 'Incubator1']], 'None', 'None', 0],
        ["S2", 10, [['IncubatorServiceResource', 'main', 'None']], 'None', 'None', 0],
        ["S3", 5, [
            ['MoverServiceResource', 'main', 'None'],
            ['IncubatorServiceResource', 'origin', 'None'],
            ['LiquidHandlerServiceResource', 'target', 'None'],
        ], 'None', 'None', 0],
        ["S4", 5, [
            ['MoverServiceResource', 'main', 'None'],
            ['IncubatorServiceResource', 'origin', 'None'],
            ['LiquidHandlerServiceResource', 'target', 'None'],
        ], 'None', 'None', 0],
        ["S5", 10, [['LiquidHandlerServiceResource', 'main', 'None']], 'None', 'None', 0],
    ]
    edges = [
        ['S3', 'S1', 1, 9999, 0],
        ['S4', 'S2', 1, 9999, 0],
        ['S5', 'S4', 1, 9999, 0],
        ['S5', 'S3', 1, 9999, 0],
    ]
    graph = [steps, edges]
    schedule_cmd = client.SchedulingService.ComputeSchedule(WorkflowGraph=graph, MaxComputationTime=5)
    while not schedule_cmd.done:
        time.sleep(.01)
    for assignment in schedule_cmd.get_responses().Schedule:
        print(assignment.ProcessStepId,
              [f"{m.Tag}->{m.MachineName}" for m in assignment.AssignedMachines],
              assignment.StartTime)


if __name__ == "__main__":
    test_call(server)
    print("Success :-)")
