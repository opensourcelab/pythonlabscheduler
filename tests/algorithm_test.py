import json
import logging
import os

import pytest
from pytest import skip
from pythonlabscheduler.sila_server import Server as SchedulerServer
from pythonlabscheduler.sila_server import Client as SchedulerClient
from os import path
import time
from sila2.framework import CommandExecutionStatus


@pytest.fixture
def server(scope='session'):
    server = SchedulerServer()
    server.start_insecure('127.0.0.1', 50071)
    yield server
    server.stop()


def run_test_series(algorithm_name: str, server: SchedulerServer = None):
    test_data_dir = path.join(path.abspath(path.dirname(__file__)), "test_data")
    client = SchedulerClient.discover(insecure=True, timeout=5)
    lab_config_file = path.join(test_data_dir, "lab_config_example.yaml")
    with open(lab_config_file, 'r') as reader:
        client.LabConfigurationController.LoadJobShopFromFile(reader.read())
    client.SchedulingService.SelectAlgorithm(algorithm_name)
    algo_info = client.SchedulingService.CurrentAlgorithm.get()
    logging.getLogger().setLevel(logging.ERROR)
    print(algo_info)
    for filename in os.listdir(test_data_dir):
        abs_filename = os.path.join(test_data_dir, filename)
        if filename.endswith('json'):
            with open(abs_filename, 'r') as reader:
                sila_wfg = json.load(reader)
            if len(sila_wfg[0]) > algo_info.MaxProblemSize:
                print(f"skipping {abs_filename} because it is too big")
                continue
            print(f"testing {abs_filename.replace('.json', '')}")
            cmd = client.SchedulingService.ComputeSchedule(sila_wfg, 5)
            while not cmd.done:
                time.sleep(.1)
            assert cmd.status == CommandExecutionStatus.finishedSuccessfully
            schedule = cmd.get_responses().Schedule
            assert schedule


def test_mip_optimizer(server, skip_this: bool = True):
    if skip_this:
        return skip("skipped for sake of time")
    try:
        run_test_series('NaivMIP', server)
    except:
        return skip("Probably SCIP support is currently not implemented")


def test_pd_heur(server):
    print("testing PD-heuristic")
    run_test_series('BottleneckPD', server)


def test_cp_solver(server):
    print("testing CP-Solver")
    run_test_series('CP-Solver', server)


def test_simple(server):
    print("testing simple solver")
    run_test_series('Simple', server)

#
# if __name__ == "__main__":
#     test_pd_heur(server)
#     test_cp_solver(server)
#     #test_mip_optimizer(False)
