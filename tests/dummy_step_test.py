import pytest
from pythonlabscheduler.sila_server import Server as SchedulerServer
from pythonlabscheduler.sila_server import Client as SchedulerClient
import json
from os import path
import time
from sila2.framework import CommandExecutionStatus


algorithm_name = "BottleneckPD"


@pytest.fixture
def server(scope='session'):
    server = SchedulerServer()
    server.start_insecure('127.0.0.1', 50071)
    yield server
    server.stop()


def test_dummy_steps(server):
    testfile = path.join(path.abspath(path.dirname(__file__)), 'test_data', 'ProteinProduction_12Plates_full.json')
    with open(testfile, 'r') as reader:
        # hast structure [Nodes, Edges]
        sila_wfg = json.load(reader)
    client = SchedulerClient.discover(insecure=True, timeout=3)
    client.SchedulingService.SelectAlgorithm(algorithm_name)
    algo_info = client.SchedulingService.CurrentAlgorithm.get()
    print(algo_info)
    expected_schedule_size = min(algo_info.MaxProblemSize, len(sila_wfg[0]))

    lab_config_file = path.join(path.abspath(path.dirname(__file__)), 'test_data', "lab_config_example.yaml")
    with open(lab_config_file, 'r') as reader:
        client.LabConfigurationController.LoadJobShopFromFile(reader.read())

    cmd = client.SchedulingService.ComputeSchedule(sila_wfg, 2)
    while not cmd.done:
        time.sleep(.1)

    assert cmd.status == CommandExecutionStatus.finishedSuccessfully
    schedule = cmd.get_responses().Schedule
    assert len(schedule) == expected_schedule_size


if __name__ == "__main__":
    test_dummy_steps(server)
